n = int(input())
i = 2
while n % i != 0:
    i += 1
print(i)

# ---------- Задание 7.9.7 ----------
# def check_number_for_simple(n: int):
#     result = True
#     for i in range(1, n + 1):
#         if i == 1 or i == n:
#             continue
#         if n % i == 0:
#             result = False
#             break
#     return result
#
# a = int(input())
# b = int(input())
#
# for i in range(a, b + 1):
#     if check_number_for_simple(i) == True:
#         if i != 1:
#             print(i)

# ---------- Задание 7.9.6 ----------

# def calc_factorial(n: int):
#     result = 1
#     for i in range(1, n + 1):
#         result *= i
#     return result
#
# n = int(input())
# sum_of_factorial = 0
#
# for i in range(1, n + 1):
#     sum_of_factorial += calc_factorial(i)
#
# print(sum_of_factorial)

# ---------- Задание 7.9.5 ----------

# print(12 // 10)
# print(12 % 10)
# print(1 // 10)
# print(1 % 10)

# def sum_of_digit(n: int):
#     sum = 0
#     while n // 10 != 0:
#         sum += n % 10
#         n = n // 10
#     sum += n
#     return sum
#
# n = int(input())
#
# while n // 10 != 0:
#     n = sum_of_digit(n)
#
# print(n)
# ---------- Задание 7.9.4 ----------

# def find_divider(n: int):
#     count = 0
#     for i in range(1, n + 1):
#         if n % i == 0:
#             count += 1
#
#     return count
#
# n = int(input())
#
# for i in range(1, n + 1):
#     count_of_divider = find_divider(i)
#     print(i, '+'*count_of_divider, sep='')

# ---------- Задание 7.9.3 ----------

# a = int(input())
# b = int(input())
# max = 0
# sum = 0
#
# for i in range(a, b + 1):
#     local_sum = 0
#     for j in range(1, i + 1):
#         if i % j == 0:
#             local_sum += j
#     if local_sum >= sum:
#         sum = local_sum
#         max = i
#
# print(max, sum)

# ---------- Задание 7.9.2 ----------

# import math
#
#
# def print_from_one_to_mid(n: int):
#     counter = 1
#     while counter != n + 1:
#         print(counter, sep='', end='')
#         counter += 1
#
#
# def print_from_mid_to_one(n: int):
#     counter = n
#     if counter == 1:
#         print('1', sep='', end='')
#     else:
#         while counter != 0:
#             print(counter, sep='', end='')
#             counter -= 1
#
#
# i = int(input())
# count = 1
#
# for n in range(1, i + 1):
#     if count == 1:
#         print(1, sep='', end='')
#     else:
#         mid = math.ceil(count // 2) + 1
#         print_from_one_to_mid(mid)
#         print_from_mid_to_one(mid - 1)
#
#     count += 2
#     print()

# i = int(input())
# counter = 1
# for n in range(1, i + 1):
#     for k in range(1, n + 1):
#         print(counter, '', end ='')
#         counter += 1
#     print()
