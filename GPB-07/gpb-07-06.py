n = int(input())
i = 1
while i != n+1:
    if 5 <= i <= 9:
        i += 1
        continue
    if 17 <= i <= 37:
        i += 1
        continue
    if 78 <= i <= 87:
        i += 1
        continue
    print(i)
    i += 1

# for i in range(2, n + 1):
#     if n % i == 0:
#         print(i)
#         break


# n = 10
# while n > 0:
#     n -= 1
#     if n == 2:
#         continue
#     print(n, end='*')

# for i in range(10):
#     print(i, end='*')
#     if i > 6:
#         break
