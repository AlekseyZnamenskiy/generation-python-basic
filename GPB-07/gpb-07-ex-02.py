# ---------- 7.ex.2.5 ----------

# n = int(input())
# count_of_three = 0
# last_digit = n % 10
# count_of_last_digit = 0
# count_of_odds = 0
# summ_of_digit_more_when_five = 0
# multiply_of_digit_more_when_seven = 1
# sum_of_zero_and_five_numbers = 0
#
# while n != 0:
#     last = n % 10
#
#     if last == 3:
#         count_of_three +=1
#
#     if last == last_digit:
#         count_of_last_digit += 1
#
#     if n % 2 == 0:
#         count_of_odds += 1
#
#     if last > 5:
#         summ_of_digit_more_when_five += last
#
#     if last > 7:
#         multiply_of_digit_more_when_seven *= last
#
#     if last == 0 or last == 5:
#         sum_of_zero_and_five_numbers += 1
#
#     n = n // 10
#
# print(count_of_three)
# print(count_of_last_digit)
# print(count_of_odds)
# print(summ_of_digit_more_when_five)
# print(multiply_of_digit_more_when_seven)
# print(sum_of_zero_and_five_numbers)

# ---------- 7.ex.2.5 ----------

# print(103 % 1000)
# n = int(input())
#
# while n % 1000 != n:
#     n = n // 10
# print(n % 10)

# ---------- 7.ex.2.4 ----------

# n = int(input())
#
# print('*' * 19)
# for i in range(1, n - 1):
#     print('*', ' ' * 17, '*', sep='')
# print('*' * 19)


# ---------- 7.ex.2.3 ----------

# n = 4
# count = 0
# maximum = -10 ** 8
#
# for i in range(1, n + 1):
#     x = int(input())
#     if x % 2 != 0:
#         count += 1
#         if x > maximum:
#             maximum = x
#
# if count > 0:
#     print(count)
#     print(maximum)
# else:
#     print('NO')

# ---------- 7.ex.2.2 ----------
#
# n = 7
# count = 0
# maximum = 0
#
# for i in range(1, n + 2):
#     x = int(input())
#     if x % 4 == 0:
#         count += 1
#     if x % 4 == 0 and x > maximum:
#         maximum = x
#
# if count > 0:
#     print(count)
#     print(maximum)
# else:
#     print('NO')

# ---------- 7.ex.2.1 ----------

# n = int(input())
# sum = 0
# while n // 10 != 0:
#     if n % 2 == 0:
#         sum += n % 10
#     n = n // 10
# if n % 2 == 0:
#     sum += n
# print(sum)
