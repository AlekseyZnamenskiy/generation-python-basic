# ---------- 08.09 -----------

# n = int(input())
# result = ''
#
# while n != 0:
#     last = n % 2
#     result = str(last) + result
#     n = int(n / 2)
#
# print(result)

# ---------- 08.09 -----------

# s = input()
# glasnie = 'ауоыиэяюе'
# soglasnie = 'бвгджзйклмнпрстфхцчшщ'
# count_of_glass = 0
# count_of_sogl = 0
#
# for i in s:
#     if str.lower(i) in glasnie:
#         count_of_glass += 1
#         print(i)
#     if str.lower(i) in soglasnie:
#         count_of_sogl += 1
#         print(i)
#
# print('Количество гласных букв равно', count_of_glass)
# print('Количество согласных букв равно', count_of_sogl)

# ---------- 08.09 -----------

# s = input()
# sum_of_equal = 0
#
# for i in range (1, len(s)):
#     if s[i] == s[i-1]:
#         sum_of_equal += 1
#
# print(sum_of_equal)

# ---------- 08.08 -----------

# s = input()
# count_of_plus = 0
# count_of_star = 0
#
# for i in s:
#     if i == '+':
#         count_of_plus += 1
#     if i == '*':
#         count_of_star += 1
#
# print('Символ + встречается', count_of_plus, 'раз')
# print('Символ * встречается', count_of_star, 'раз')

# ---------- 08.07 -----------

# s = input()
# digits = '0123456789'
# result = False
#
# for i in s:
#     if i in digits:
#         result = True
#         break
#
# if result == True:
#     print('Цифра')
# else:
#     print('Цифр нет')

# ---------- 08.06 -----------

# s = input()
# n = int(s)
# sum = 0
#
# while n != 0:
#     last = n % 10
#     sum += last
#     n //= 10
#
# print(sum)

# ---------- 08.05 -----------

# a = input()
# b = input()
# c = input()
#
# print(b[0], a[0], c[0], sep='')

# ---------- 08.04 -----------

# s = input()
#
# for i in range(1, len(s)+1):
#     print(s[-i])

# ---------- 08.03 -----------

# s = input()
#
# for i in range (0, len(s), 2):
#     print(s[i])

# ---------- 08.02 -----------

# s = "In 2010, someone paid 10k Bitcoin for two pizzas."
#
# print(s[-10])

# ---------- 08.01 -----------

# s = "In 2010, someone paid 10k Bitcoin for two pizzas."

# print(s[7])